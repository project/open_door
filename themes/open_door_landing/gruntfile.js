/* global module */
module.exports = function (grunt) {
  'use strict';

  grunt.loadNpmTasks('grunt-contrib-less');
  grunt.loadNpmTasks('grunt-contrib-watch');

  grunt.initConfig({
    pkg: grunt.file.readJSON('package.json'),
    less: {
      default: {
        options: {
          paths: ['css'],
          compress: false,
          ieCompat: true,
          sourceMap: true,
        },
        files: [{
          expand: true,
          cwd: 'less',
          src: ['**/*.less', '!**/_*.less'],
          dest: 'css/',
          ext: '.css'
        }]
      }
    },
    watch: {
      options: {
        livereload: true
      },
      less: {
        files: ['less/**/*.less'],
        tasks: ['less:default']
      }
    }
  });

  grunt.task.registerTask('default', ['watch']);
};
