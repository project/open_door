<?php

/**
 * @file
 * Theme settings form.
 */

/**
 * Implements hook_form_system_theme_settings_alter().
 */
function open_door_landing_form_system_theme_settings_alter(&$form, &$form_state) {

  $form['open_door'] = [
    '#type' => 'details',
    '#title' => t('Open door'),
    '#open' => TRUE,
  ];

  $form['open_door']['color_scheme'] = [
    '#type' => 'radios',
    '#title' => t('Color scheme'),
    '#options' => [
      'freeze' => t('Freeze'),
      'eco' => t('Eco'),
      'berry' => t('Berry'),
    ],
    '#default_value' => theme_get_setting('color_scheme'),
  ];

  $form['#submit'][] = 'open_door_landing_form_system_theme_settings_submit';
}

/**
 * Theme settings form submit handler.
 */
function open_door_landing_form_system_theme_settings_submit(&$form, &$form_state) {
  \Drupal::service('cache_tags.invalidator')->invalidateTags(['snippet_view']);
}
