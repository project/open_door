/**
 * @file
 * Open door landing behaviors.
 */

(function ($, Drupal) {

  'use strict';

  Drupal.behaviors.openDoorLanding = {
    attach: function (context, settings) {

      var $window = $(window);

      // Status messages.
      $('#messages-modal').modal();

      // Scrolled point.
      var num = 50;
      var $nav = $('nav');

      var setScrolledClass = function () {
        $nav.toggleClass('scrolled', $window.scrollTop() > num);
      };
      $window.scroll(setScrolledClass);
      setScrolledClass();

      // Header animation.
      var $header = $('header');
      var setHeaderHeight = function () {
        $header.css('height', $window.height() + 'px');
      };
      $window.resize(Drupal.debounce(setHeaderHeight, 100));
      setHeaderHeight();
      $('.lr').removeClass('lr-animation').addClass('lr-animation');

      // Smooth scrolling.
      var $navBar = $('.main-navbar').find('.navbar-collapse');
      $('a[href*="#"]').not('[role="button"]').click(function () {
        $navBar.removeClass('in');
        if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location.hostname == this.hostname) {
          var target = $(this.hash);
          var top = target.length ? target.offset().top : 0;
          $('html, body').animate({
            scrollTop: top
          }, 700);
          return false;
        }
      });

      // Screens carousel.
      $('#screens').find('.slider').slick({
        slidesToShow: 3,
        slidesToScroll: 1,
        dots: false,
        responsive: [{
          breakpoint: 992,
          settings: {
            slidesToShow: 2
          }
        }, {
          breakpoint: 768,
          settings: {
            slidesToShow: 1
          }
        }]
      });

      // Scroll animations.
      $('*[data-animate]').waypoint(function () {
        var $element = $(this.element);
        $element.toggleClass('animated ' + $element.data('animate'));
      }, {offset: '95%'})

    }
  };

}(jQuery, Drupal));
