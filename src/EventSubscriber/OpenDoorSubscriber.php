<?php

namespace Drupal\open_door\EventSubscriber;

use Drupal\Core\Cache\CacheTagsInvalidatorInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Session\AccountProxyInterface;
use Drupal\Core\Url;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpKernel\Event\GetResponseEvent;
use Symfony\Component\HttpKernel\KernelEvents;

/**
 * Open door event subscriber.
 */
class OpenDoorSubscriber implements EventSubscriberInterface {

  /**
   * Current logged in user.
   *
   * @var \Drupal\Core\Session\AccountProxyInterface
   */
  protected $currentUser;

  /**
   * The config factory used by the config entity query.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * The cache tag invalidator.
   *
   * @var \Drupal\Core\Cache\CacheTagsInvalidatorInterface
   */
  protected $cacheTagInvalidator;

  /**
   * Constructs event subscriber.
   *
   * @param \Drupal\Core\Session\AccountProxyInterface $current_user
   *   Current logged in user.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config storage.
   * @param \Drupal\Core\Cache\CacheTagsInvalidatorInterface $cache_tag_invalidator
   *   The cache tag invalidator.
   */
  public function __construct(AccountProxyInterface $current_user, ConfigFactoryInterface $config_factory, CacheTagsInvalidatorInterface $cache_tag_invalidator) {
    $this->currentUser = $current_user;
    $this->configFactory = $config_factory;
    $this->cacheTagInvalidator = $cache_tag_invalidator;
  }

  /**
   * Kernel request event handler.
   *
   * @param \Symfony\Component\HttpKernel\Event\GetResponseEvent $event
   *   Response event.
   */
  public function onKernelRequest(GetResponseEvent $event) {
    $color = $event->getRequest()->get('color');
    if ($color && in_array($color, ['freeze', 'eco', 'berry']) && $this->currentUser->hasPermission('administer themes')) {
      $config = $this->configFactory->getEditable('open_door_landing.settings');
      $favicon = [
        'use_default' => FALSE,
        'path' => drupal_get_path('theme', 'open_door_landing') . '/images/' . $color . '/favicon.png',
        'mimetype' => 'image/png',
      ];
      $config
        ->set('color_scheme', $color)
        ->set('favicon', $favicon)
        ->save();
      $event->setResponse(new RedirectResponse(Url::fromRoute('<front>')->toString()));
      $this->cacheTagInvalidator->invalidateTags(['snippet_view']);
    }
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    return [KernelEvents::REQUEST => ['onKernelRequest']];
  }

}
