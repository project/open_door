<?php

/**
 * @file
 * Enables modules and site configuration for an Open door installation profile.
 */

/**
 * Implements hook_snippet_view_alter().
 */
function open_door_snippet_view_alter(array &$build) {
  $scheme_path = drupal_get_path('theme', 'open_door_landing') . '/images/' . theme_get_setting('color_scheme', 'open_door_landing');
  $build['snippet']['#context']['images_dir'] = file_create_url($scheme_path);
}

/**
 * Implements hook_page_top().
 */
function open_door_page_top(array &$page_top) {
  if (\Drupal::theme()->getActiveTheme()->getName() == 'open_door_landing') {
    $page_top['toolbar']['#access'] = FALSE;
  }
}

/**
 * Implements hook_module_implements_alter().
 */
function open_door_module_implements_alter(&$implementations, $hook) {
  if ($hook == 'page_top') {
    // Move hook_page_top implementation to the end so it will be called after
    // toolbar module.
    unset($implementations['open_door']);
    $implementations['open_door'] = FALSE;
  }
}
