api = 2
core = 8.x
projects[drupal] = 8.2.6

projects[open_door][type] = profile
projects[open_door][download][type] = git
projects[open_door][download][branch] = 8.x-3.7
